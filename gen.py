#!/usr/bin/env python3


import itertools


# :::::::::
#   Helper functions for working with calculation generators
# :::::::::

def gtee(calcfuncs, iterable):
    '''Consume iterable distributing its values to calculations in calcfuncs.

    calcfuncs should be a sequence/iterable of pushfunc, resultfunc tuples of
    the type returned by gwrap().

    All calculations will be computed in a single pass over iterable..

    >>> # Let's create some uninteresting data
    >>> data = [3,4,5]

    >>> # Setup desired calulations
    >>> calcs = [gwrap(g) for g in (gsum(), gmean(), gmax(), gvar())]

    >>> # Compute the sum, mean, max and var of some simple data
    >>> gtee(calcs, [3, 4, 5])
    [12, 4.0, 5, 1.0]
    '''
    pushfuncs = []
    resultfuncs = []
    for push, result in calcfuncs:
        pushfuncs.append(push)
        resultfuncs.append(result)

    for val in iterable:
        for push in pushfuncs:
            push(val)

    return [result() for result in resultfuncs]

def gwrap(gen, key=None, predicate=None):
    '''Initialise calculation generator and return push and result functions.

    If provided, key and predicate are functions used to map and filter the
    input values respectively.

    Usage:

    >>> def gsum(initial=0):
    ...     yield lambda : total
    ...     total = initial
    ...     while True:
    ...         total += yield

    >>> push, result = gwrap(gsum())
    >>> push(3)
    >>> push(5)
    >>> result()
    8
    >>> push(2)
    >>> result()
    10
    '''
    resultfunc = next(gen)
    # Factored generators yield a push function and a result function
    try:
        push, resultfunc = resultfunc
    except TypeError:
        # For other generators push is just gen.send
        push = gen.send
        next(gen) # Proceed to the first waiting yield call

    if key is None and predicate is None:
        pushwrap = push
    elif key is None:
        pushwrap = lambda val: push(val) if predicate(val) else None
    elif predicate is None:
        pushwrap = lambda val: push(key(val))
    else:
        pushwrap = lambda val: push(key(val)) if predicate(val) else None
    return pushwrap, resultfunc

# :::::::::
#   Examples of calculation generators
# :::::::::

def gmax():
    '''Max of sent values'''
    yield lambda : currentmax
    currentmax = yield
    while True:
        newval = yield
        if newval > currentmax:
            currentmax = newval

def gsum(initial=0):
    '''Sum of sent values'''
    yield lambda : total
    total = initial
    while True:
        total += yield

def gmean():
    '''Mean of sent values'''
    yield lambda : total / count
    total = 0
    for count in itertools.count():
        total += yield

def gvar(correction=1):
    '''Variance of sent values. Scales by (N - correction)'''
    yield lambda: totalvar / (count - 1 - correction) if count >= 1 else 0
    mean = 0
    totalvar = 0
    for count in itertools.count(1):
        newval = yield
        oldmean = mean
        mean += (newval - mean) / count
        totalvar += (newval - oldmean) * (newval - mean)

def gmeanvar(correction=1):
    '''Variance and mean of sent values. Scales by (N - correction)'''
    fvar = lambda: (totalvar / (count - 1 - correction) if count >= 1 else 0)
    yield lambda: (mean, fvar())
    mean = 0
    totalvar = 0
    for count in itertools.count(1):
        newval = yield
        oldmean = mean
        mean += (newval - mean) / count
        totalvar += (newval - oldmean) * (newval - mean)

# :::::::::
#   How to make a generator that derives its result from another
# :::::::::

def gvar(correction=1):
    '''Variance of sent values. Scales by (N - correction)'''
    push, result = gwrap(gmeanvar(correction))
    yield (push, lambda: result()[1])

def gstd(correction=1):
    '''Variance of sent values. Scales by (N - correction)'''
    push, result = gmeanvar(correction)
    yield (push, lambda: result()[1] ** 0.5)

def gconst(val=0):
    '''Ignores sent values and returns 0'''
    yield lambda : val
    while True:
        yield

# :::::::::
#   Examples of iterator calculations
# :::::::::

def isum(iterable):
    '''Sum of iterable'''
    result = 0
    for newval in iterable:
        result += newval
    return result

def imean(iterable):
    '''Mean of iterable'''
    total = 0
    for count, newval in enumerate(iterable, 1):
        total += newval
    return total / count

def imax(iterable):
    '''Max of iterable'''
    result = float('-inf')
    for newval in iterable:
        if newval > result:
            result = newval
    return result

def ivar(iterable, correction=1):
    '''Variance of iterable. Scales by (N - correction)'''
    mean = 0
    totalvar = 0
    for count, newval in enumerate(iterable, 1):
        oldmean = mean
        mean += (newval - mean) / count
        totalvar += (newval - oldmean) * (newval - mean)
    return totalvar / (count - correction) if count >= 2 else 0

def iconst(iterable, const=0):
    '''Consumes iterable and returns 0'''
    for _ in iterable:
        pass
    return const


if __name__ == "__main__":
    import sys
    args = sys.argv[1:]

    # Test
    if not args:
        import doctest
        doctest.testmod()

    # Profile and time
    else:
        import cProfile
        import random
        import timeit
        N = int(args[0])
        number = int(args[1]) if len(args) > 1 else 1
        data = [random.random() for _ in range(N)]
        print('Calculate with %i random variables' % N)

        def gcalcs():
            names = ('Max',  'Mean',  'Sum',  'Var',  'Const')
            gens =  (gmax(), gmean(), gsum(), gvar(), gconst())
            calcs = [gwrap(g) for g in gens]
            for name, val in zip(names, gtee(calcs, data)):
                print(name, val)

        def icalcs():
            names = ('Max', 'Mean', 'Sum', 'Var', 'Const')
            funcs = (imax,  imean,  isum,  ivar,  iconst )
            for name, func in zip(names, funcs):
                print(name, func(data))

        def gzero_prof(N):
            calcs = [gwrap(gconst()) for n in range(N)]
            gtee(calcs, data)

        def izero_prof(N):
            for n in range(N):
                iconst(data)

        def nformat(val):
            try:
                return ' '.join(map(nformat, val))
            except:
                return '%3.3f' % val

        tg = timeit.timeit('gcalcs()', 'from __main__ import gcalcs', number=number)
        ti = timeit.timeit('icalcs()', 'from __main__ import icalcs', number=number)

        tg0s = []
        ti0s = []
        Nzero = [1, 2, 4, 8, 16, 32, 64, 128]
        for n in Nzero:
            tg0s.append(timeit.timeit('gzero_prof(%i)' % n,
                'from __main__ import gzero_prof', number=number))
            ti0s.append(timeit.timeit('izero_prof(%i)' % n,
                'from __main__ import izero_prof', number=number))

        cProfile.run('gcalcs()', sort='cumulative')
        cProfile.run('icalcs()', sort='cumulative')

        print('\n   timeit results:')
        print('Time (Gen) :', nformat(tg), 'seconds')
        print('Time (Iter):', nformat(ti), 'seconds')

        print('\n timeit results for dummy calculations')
        print('Using %s calculations' % Nzero)
        print('Time0 (Gen) :', nformat(tg0s), 'seconds')
        print('Time0 (Iter):', nformat(ti0s), 'seconds')
